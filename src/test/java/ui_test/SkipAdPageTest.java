package ui_test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.api.QrServiceResponseEntity;
import utilities.driver.DriverManager;
import utilities.log.Log;

import static utilities.properties.ApplicationProperties.BASE_URL_FOR_REDIRECT;

public class SkipAdPageTest {
    WebDriver driver;
    SkipAdPage skipAdPage;
    QrServiceResponseEntity entity;
    String urlForTest;

    @BeforeClass(alwaysRun = true)
    public void setUp(ITestContext context) {
        driver = DriverManager.getDriver();
        skipAdPage = new SkipAdPage(driver);

        // specific feature for input link
        urlForTest = "https://" + context.getAttribute("urlForTest").toString();
        Log.Info("url for test from context - " + urlForTest);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        DriverManager.clearDriver();
        skipAdPage = null;
    }


    @Test(dependsOnGroups = {"createQr"}, groups = {"mainFlowLink"}, priority = 1, invocationCount = 10)
    public void goToLink(ITestContext context){
        Log.Info("Start test for redirect");
        entity = (QrServiceResponseEntity) context.getAttribute("entity");
        driver.get(BASE_URL_FOR_REDIRECT + entity.getId());
        Log.Info("Go to page - " + BASE_URL_FOR_REDIRECT + entity.getId());
        String url = driver.getCurrentUrl();
        Log.Info("Current url - " + url);
        Assert.assertEquals(url, urlForTest);
    }

    @Test(dependsOnGroups = {"createQr"}, groups = {"mainFlowLink"}, priority = 2)
    public void skipAdvertisementTest(ITestContext context){
        entity = (QrServiceResponseEntity) context.getAttribute("entity");
        driver.get(BASE_URL_FOR_REDIRECT + entity.getId());
        skipAdPage.waitForPageLoad(10);
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, BASE_URL_FOR_REDIRECT + entity.getId());
        Assert.assertTrue(skipAdPage.isAdBlockDisplayed());
        skipAdPage.clickBySkipAd();
        skipAdPage.waitForPageLoad(10);

        boolean isContain = false;
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
            if (driver.getCurrentUrl().equals(urlForTest)){
                isContain=true;
            }
            System.out.println(String.format("handle: %s, url: %s", handle, driver.getCurrentUrl()));
        }

        Assert.assertTrue(isContain);
    }
}
