package ui_test;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.api.QrServiceResponseEntity;
import utilities.driver.DriverManager;
import utilities.properties.ApplicationProperties;

import static org.testng.Assert.*;

public class LandingPageTest {
    WebDriver driver;
    LandingPage landingPage;
    String urlForTest = "www.baeldung.com/java-enum-values";
    QrServiceResponseEntity entity;

    @BeforeClass(alwaysRun = true)
    public void init(){
        driver = DriverManager.getDriver();
        landingPage = new LandingPage(driver);
    }

    @AfterClass(alwaysRun = true)
    public void down(){
        DriverManager.clearDriver();
        landingPage = null;
    }


    @Test(priority = 1, groups = {"createQr", "mainFlowLink"})
    public void qrFromLinkFlow(ITestContext context){
        driver.get(ApplicationProperties.LANDING_PAGE);
        landingPage.selectInputType(InputType.LINK);
        landingPage.inputIntoTextArea(urlForTest);
        landingPage.clickByGenerateBtn();
        entity = landingPage.getEntity();
        context.setAttribute("entity", entity);
        context.setAttribute("urlForTest", urlForTest);
    }

}