package utilities.runner;

import org.testng.annotations.Test;

import java.io.IOException;

public class RunnerTest {
    @Test
    public void testRunTestGroupInPackage() throws IOException, ClassNotFoundException {
        Runner.runTestGroupInPackage("mainFlowLink", "test");
    }

}