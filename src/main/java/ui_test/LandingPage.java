package ui_test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utilities.api.QrServiceResponseEntity;

public class LandingPage extends BasePage{

    public static final String CHOOSE_LINK_XPATH = "//*[@id='qr-type-selector']/div/ul/li[1]/a";

    public static final String INPUT_LINK_AREA_XPATH = "//*[@id='linkTextArea']";

    public static final String GENERATE_QR_CODE_BTN_XPATH = "//*[@id='link']/div[1]/form/div/div[2]/input";


    public static final String ENTRY_ID_XPATH = "//input[@name='entry_id']";
    public static final String ENTRY_HASH_XPATH = "//input[@name='entry_hash']";


    @FindBy(how = How.XPATH,xpath = CHOOSE_LINK_XPATH)
    private WebElement choseLink;

    @FindBy(how = How.XPATH,xpath = INPUT_LINK_AREA_XPATH)
    private WebElement inputArea;

    @FindBy(how = How.XPATH,xpath = GENERATE_QR_CODE_BTN_XPATH)
    private WebElement generateBtn;

    @FindBy(how = How.XPATH,xpath = ENTRY_ID_XPATH)
    private WebElement entityId;

    @FindBy(how = How.XPATH,xpath = ENTRY_HASH_XPATH)
    private WebElement entityHash;


    public LandingPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.action = new Actions(driver);
        driver.manage().window().maximize();
    }

    public void selectInputType(InputType inputType){
        waitForPageLoad(10);
        switch (inputType){
            case LINK:
                clickByElement(choseLink);
        }
    }

    public void inputIntoTextArea(String text){
        waitForPageLoad(10);
        inputArea.sendKeys(text);
    }

    public void clickByGenerateBtn(){
        waitForPageLoad(10);
        clickByElement(By.xpath(GENERATE_QR_CODE_BTN_XPATH));
    }

    public QrServiceResponseEntity getEntity(){
        waitForPageLoad(10);
        QrServiceResponseEntity entity = new QrServiceResponseEntity();
        entity.setId(Integer.parseInt(entityId.getAttribute("value")));
        entity.setHash(entityHash.getAttribute("value"));
        return entity;
    }
}
