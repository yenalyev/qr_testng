package ui_test;

public class SiteCreationException extends RuntimeException {

    public SiteCreationException(String message, Throwable e) {
        super(message, e);
    }

}
