package ui_test;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utilities.driver.DriverManager;
import utilities.properties.CommonProperties;
import utilities.properties.PropertyLoader;

import java.util.List;

public class BasePage {
    public WebDriver driver;
    public Actions action;

    public BasePage(WebDriver driver){

        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.action = new Actions(driver);
        driver.manage().window().maximize();
    }

    private static final long DEFAULT_WAIT_TIMEOUT = 60;

    /**
     * Effectively a getter for the web driver wait object. Just makes code more readable to not have "get" at the front.
     * <p>
     * E.g. site.webDriverWait().until(...)
     *
     * @return a web driver wait object
     */
    public static WebDriverWait webDriverWait(long timeoutInSeconds) {
        return new WebDriverWait(DriverManager.getDriver(), timeoutInSeconds);
    }

    /**
     * Like <code>webDriverWait(long timeoutInSeconds) but with the default timeout in seconds.</code>
     *
     * @return a web driver wait object
     */
    public static WebDriverWait webDriverWait() {

        long timeoutInSeconds = DEFAULT_WAIT_TIMEOUT;

        String timeoutProperty = PropertyLoader.getProperty(CommonProperties.SELENIUM_DRIVER_WAIT_TIMEOUT);

        if (timeoutProperty != null) {
            try {
                timeoutInSeconds = Long.parseLong(timeoutProperty);
            } catch (NumberFormatException e) {
                throw new SiteCreationException("The format of " + timeoutProperty + " for the driver_wait_timeout property is invalid", e);
            }
        }

        return webDriverWait(timeoutInSeconds);
    }

    public static void navigateBack() {
        DriverManager.getDriver().navigate().back();
    }

    public static boolean isElementPresent(By locator) {
        return DriverManager.getDriver().findElements(locator).size() > 0;
    }

    public static boolean isElementVisible(By locator) {
        try {
            return DriverManager.getDriver().findElement(locator).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void click(By locator) {
        webDriverWait().until(ExpectedConditions.elementToBeClickable(locator));
        for (int i = 0; i < 10; i++) {
            try {
                findElement(locator).click();
            } catch (StaleElementReferenceException e) {
                continue;
            }
            break;
        }
    }

    public static void click(WebElement element) {
        webDriverWait().until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public static void clickRandomLink(By locator) {
        WebElement container = findElement(locator);
        List<WebElement> links = container.findElements(By.tagName("a"));
        if (!links.isEmpty()) {
            click(links.stream().findAny().get());
        } else {
            throw new NotFoundException("Link element could not be found in container located " + locator);
        }
    }

    public static void selectElementFromDropDown(By element, String item) {
        webDriverWait().until(ExpectedConditions.presenceOfElementLocated(element));
        Select select = new Select(findElement(element));
        select.selectByVisibleText(item);
    }

    public static void refreshPage() {
        DriverManager.getDriver().navigate().refresh();
    }

    public static void selectElementFromDropDownByIndex(By element, int index) {
        webDriverWait().until(ExpectedConditions.presenceOfElementLocated(element));
        Select select = new Select(findElement(element));
        select.selectByIndex(index);
    }

    public static void appendTextBoxDetails(By elementLocator, String message) {
        webDriverWait().until(ExpectedConditions.presenceOfElementLocated(elementLocator));
        findElement(elementLocator).sendKeys(message);
    }

    public static void enterTextBoxDetails(By elementLocator, String message) {
        webDriverWait().until(ExpectedConditions.presenceOfElementLocated(elementLocator));
        clearTextBoxDetails(elementLocator);
        findElement(elementLocator).sendKeys(message);
    }

    public static void clearTextBoxDetails(By elementLocator) {
        webDriverWait().until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
        findElement(elementLocator).clear();
    }

    public static void waitForUrlToChange(String url) {
        webDriverWait().until((ExpectedCondition<Boolean>) d -> !(d != null && d.getCurrentUrl().equals(url)));
    }

    public static void goToURL(String webAddress) {
        DriverManager.getDriver().get(webAddress);
    }

    public static WebElement findElement(By by) {
        return DriverManager.getDriver().findElement(by);
    }

    private static void closeWindow() {
        DriverManager.getDriver().close();
    }


    public void waitForPageLoad(int seconds){
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }
    public void switchToNextTab(){
        action.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
    }


    /**
     * Wait and click by element
     * @param element
     */

    public void clickByElement(WebElement element){
        waitForPageLoad(10);
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                element.click();
                break;
            } catch (ElementNotInteractableException e) {
                System.out.println("missing");
            }
        }
    }

    /**
     * Wait and click by element
     * @param by
     */

    public void clickByElement(By by){
        waitForPageLoad(10);
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                findElement(by).click();
                break;
            } catch (ElementNotInteractableException e) {
                System.out.println("missing");
            }
        }
    }
}
