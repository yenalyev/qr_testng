package ui_test;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SkipAdPage extends BasePage{

    public static final String SKIP_AD_BTN_XPATH = "//a[contains(text(),'Skip advertisement')]";
    public static final String AD_BLOCK = "//ins[@class='adsbygoogle']";

    @FindBy(how = How.XPATH, xpath = SKIP_AD_BTN_XPATH)
    private WebElement skipAdBtn;

    @FindBy(how = How.XPATH, xpath = AD_BLOCK)
    private WebElement adBlock;

    public SkipAdPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.action = new Actions(driver);
        driver.manage().window().maximize();
    }

    public void clickBySkipAd(){
        waitForPageLoad(10);
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                skipAdBtn.click();
                break;
            } catch (ElementNotInteractableException e) {
                System.out.println("missing");
            }
        }
    }

    public boolean isAdBlockDisplayed(){
        return adBlock.isDisplayed();
    }


}
