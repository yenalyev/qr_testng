package utilities.driver;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utilities.log.Log;
import utilities.properties.CommonProperties;
import utilities.properties.PropertyLoader;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.concurrent.TimeUnit;

public class DriverFactory {


    public static WebDriver createInstance(String browser) {
        WebDriver instance = null;

        switch (browser) {
            case "CHROME":
                instance = initWebDriver("CHROME");
                break;
            case "CHROMER":
                instance = initRemoteDriver("chrome");
                break;
            case "FIREFOX":
                instance = initWebDriver("FIREFOX");
                break;
            case "FIREFOXR":
                instance = initRemoteDriver("firefox");
                break;
            case "IE":
                instance = initWebDriver("IE");
                break;
            default:
                System.err.println("No browser specified");
        }
        return instance;
    }

    private static WebDriver initWebDriver(String driverName) {
        switch (driverName){
            case "CHROME":
                //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
                ChromeOptions options = new ChromeOptions();
                options.setHeadless(true);
                options.addArguments("disable-infobars"); // disabling infobars
                options.addArguments("--disable-extensions"); // disabling extensions
                options.addArguments("--disable-gpu"); // applicable to windows os only
                options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
                options.addArguments("--no-sandbox"); // Bypass OS security model
                WebDriver chromeDriver = new ChromeDriver(options);
                chromeDriver.manage().window().maximize();
                return chromeDriver;

            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                WebDriver firefoxDriver = new FirefoxDriver();
                firefoxDriver.manage().window().maximize();
                return firefoxDriver;

            default:
                WebDriver driver = new ChromeDriver();
                setImplicitlyWait(driver, 30);
                driver.manage().window().maximize();
                return driver;
        }

    }
    static void setImplicitlyWait(WebDriver driver, long value) {
        driver.manage().timeouts().implicitlyWait(value, TimeUnit.SECONDS);
    }

    public static RemoteWebDriver initRemoteDriver(String browser){
        Log.Info("Start remoteVebDriver for - " + browser);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(browser);
        //capabilities.setVersion(PropertyLoader.getProperty(CommonProperties.BROWSER_VERSION));
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);
        capabilities.setJavascriptEnabled(true);
        capabilities.setCapability("name", PropertyLoader.getProperty(CommonProperties.PROJECT_NAME));

        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    URI.create(PropertyLoader.getProperty(CommonProperties.SELENOID_URL)).toURL(),
                    capabilities
            );
            driver.manage().window().setSize(new Dimension(1920, 1080));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;

    }

}
