package utilities.driver;


public class DriverMode {
    private static DriverMode instance;
    private DriverMode () {};
    public static DriverMode getInstance() {
        if (instance == null) {
            instance = new DriverMode();
        }
        return instance;
    }

    private static boolean remote;
    public static boolean isRemote() {
        return remote;
    }
    public static void setRemote(boolean remote) {
        DriverMode.remote = remote;
    }
}
