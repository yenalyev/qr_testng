package utilities.driver;

public enum BrowserType {
    ChromeLocal,
    ChromeRemote
}
