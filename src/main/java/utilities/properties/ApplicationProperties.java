package utilities.properties;

public class ApplicationProperties {
    public static final String API_PATH = "https://me-qr.com/entry/link/ajax/new";
    public static final String LANDING_PAGE = "https://me-qr.com";
    public static final String BASE_URL_FOR_REDIRECT = "https://me-qr.com/";
}
