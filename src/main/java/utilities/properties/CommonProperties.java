package utilities.properties;

public class CommonProperties {
    public final static String BROWSER_TYPE = "browser.type";
    public final static String BROWSER_VERSION = "browser.version";

    public final static String PROJECT_NAME = "project.name";

    public static final String SCREENSHOT_OFF = "screenshot.off";

    public final static String SELENOID_URL = "selenoid.url";
    public final static String SELENIUM_DRIVER_WAIT_TIMEOUT = "selenium.driver.wait.timeout";
    public final static String SELENOID_ENABLE_VNC = "selenium.driver.wait.timeout";

}
