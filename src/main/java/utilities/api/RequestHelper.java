package utilities.api;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import utilities.properties.ApplicationProperties;

import java.net.MalformedURLException;
import java.net.URL;

public class RequestHelper {
    public static Response createPath(String link, int type) throws MalformedURLException {
        RestAssured.defaultParser = Parser.JSON;
        return
                RestAssured.
                        given().
                        contentType("multipart/form-data").
                        multiPart("link", link).
                        multiPart("entry", "").
                        multiPart("type", type).
                        post(new URL(ApplicationProperties.API_PATH));
    }

    public static QrServiceResponseEntity getQrServiceEntity(String link, int type) throws MalformedURLException {
        QrServiceResponseEntity entity = new QrServiceResponseEntity();
        Response response = createPath(link, type);
        entity.setId(response.getBody().jsonPath().getInt("id"));
        entity.setHash(response.getBody().jsonPath().getString("hash"));
        entity.setExists(response.getBody().jsonPath().getBoolean("exists"));
        return entity;
    }
}
