package utilities.api;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QrServiceResponseEntity {
    private int id;
    private String hash;
    private boolean exists;
}

