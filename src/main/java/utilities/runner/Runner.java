package utilities.runner;

import org.testng.TestNG;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Runner {
    public static void runTestGroupInPackage(String testGroup, String suiteName){

        System.setProperty("customName", "qrTest " + testGroup);
        XmlSuite suite = new XmlSuite();
        suite.setName(suiteName);
        suite.setParallel(XmlSuite.ParallelMode.CLASSES);
        suite.setThreadCount(1);

        HashMap<String, String> suiteParams = new HashMap<>();
        suiteParams.put("containersQty", "1");


        suite.setParameters(suiteParams);
        XmlTest test = new XmlTest(suite);
        test.setName("tempTest");

        suite.addIncludedGroup(testGroup);

        List<XmlPackage> testPackages = new ArrayList<>();
        testPackages.add(new XmlPackage("ui_test"));

        test.setPackages(testPackages);
        List<XmlSuite> list = new ArrayList<>();
        list.add(suite);

        System.out.println(suite.toXml());

        TestNG tng = new TestNG();
        tng.setXmlSuites(list);


        tng.run();
    }
}
